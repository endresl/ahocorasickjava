import java.util.ArrayList;

public class Node {

    String name;
    boolean realEndPoint;
    String color, thickness;
    char edge;
    Node parent;
    ArrayList<Node> childs;
    Node hitNode;

    public Node(String name, char edge, Node parent) {
        thickness = "";
        color = "black";
        realEndPoint = false;
        this.parent = parent;
        this.name = name;
        this.edge = edge;
        childs = new ArrayList<>();
    }

    public int getLevel(){
        if(edge == ' '){
            return 0;
        }else{
            return name.length();
        }
    }

    public void addChild(Node node){
        childs.add(node);
    }

    public void realEndPoint(){
        realEndPoint = true;
        endPoint(", fill = green", this);
    }

    public void endPoint(String fill, Node hitHode){
        this.hitNode = hitHode;
        thickness = "very thick" + fill;
        color = "green";
    }

    public boolean isEndPoint(){
        return color == "green";
    }

    public boolean isRealEndPoint(){
        return realEndPoint;
    }

    @Override
    public String toString() {
        return name;
    }
}
