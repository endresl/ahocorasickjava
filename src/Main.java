import javafx.util.Pair;

import java.util.*;
import java.awt.datatransfer.StringSelection;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;

public class Main {

    static Node root;
    static boolean withFailureLinks;
    static boolean withTarget;
    static String target;
    static StringBuilder tree;
    static ArrayList<String> words;
    static String input;

    static HashMap<String, ArrayList<Integer>> results = new HashMap<>();
    static HashMap<Node, Node> failureLinks = new HashMap<>();
    static int depth;

    public static void main(String[] args) {

        input = "";
        withFailureLinks = false;
        withTarget = false;

        words = new ArrayList<>();

        for (int i = 0; i < args.length; i++) { //cmd line parser
            input += " " + args[i];
            if (args[i].startsWith("-")) {
                if (args[i].substring(1).equals("failure")) {
                    withFailureLinks = true;
                }
                if (args[i].substring(1).equals("target") && i + 1 < args.length) {
                    withFailureLinks = true;
                    withTarget = true;
                    target = args[++i];
                    input += " " + target;
                }

            } else {
                results.put(args[i], new ArrayList<>());
                words.add(args[i]);
            }
        }


        //words = Arrays.asList(args);

        //find depth, depth = longest word
        depth = words.get(0).length();
        for (int i = 1; i < words.size(); i++) {
            if (words.get(i).length() > depth) {
                depth = words.get(i).length();
            }
        }

        tree = new StringBuilder();
        root = new Node("root", ' ', null);

        //Baum in latex !NEU! jetzt rekursiv
        for (String word : words) {
            addWordToTree(word.toLowerCase());
        }

        printFullTreeLatex();

        if (withTarget) {
            search();
        }

        printSearchResults();

        String myString = tree.toString();
        StringSelection stringSelection = new StringSelection(myString);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(stringSelection, null);
        System.out.println("Aho-Corasick search tree copied to your clipboard!\nPaste it into your Latex editor, make sure you are using \\usepackage{tikz} and \\usepackage{tikz-qtree}.");
    }

    private static void printSearchResults() {
        if(withTarget) {
            tree.append("\n\nSearch results for " + words.toString() + " in ``" + target + "'': \\\\ \n");
            for (String word : words) {
                tree.append("\\itab{``" + word + "'' found at position(s):}\\tab{" + results.get(word).toString() + "} \\\\ \n");
            }
        }
        tree.append("\\\\\n");
        tree.append("This output (tree and results) was generated using:\\\\ ``java -jar AhoCorasickSearchTree.jar " + input +"''");
        tree.append("\\\\\n\n");
        tree.append("Source code and .jar available at: \\url{https://gitlab2.cip.ifi.lmu.de/endresl/ahocorasickjava/}");
    }

    private static void search() {
        Node v = root;
        boolean hasChildWithEdge = true;
        int k = 0;
        while (k < target.length()) {
            hasChildWithEdge = true;
            int i = k;
            while (hasChildWithEdge) {
                hasChildWithEdge = false;
                for (Node child : v.childs) {
                    if (i < target.length() && child.edge == target.charAt(i)) {
                        k = i;
                        v = child;
                        if (v.isEndPoint()) {
                            results.get(v.hitNode.name).add(i - (v.hitNode.name.length() - 1));
                        }
                        hasChildWithEdge = true;
                    }
                }
                if (hasChildWithEdge) {
                    i++;
                }
            }

            if (v.edge != ' ') {
                k = i;
                v = failureLinks.get(v);
            } else {
                k++;
            }
        }
    }

    private static void addWordToTree(String word) {
        addToTree(word, root, 0);
    }

    private static void addToTree(String word, Node startNode, int curWordIndex) {
        for (int i = 0; i < startNode.childs.size(); i++) {
            if (curWordIndex < word.length() && word.charAt(curWordIndex) == startNode.childs.get(i).edge) {
                addToTree(word, startNode.childs.get(i), ++curWordIndex); //step down tree until no child with next letter found
                return;
            }
        }
        for (int i = curWordIndex; i < word.length(); i++) { //add remaining chars to current node
            Node newNode = new Node(word.substring(0, i + 1), word.charAt(i), startNode);
            if (words.contains(newNode.name)) //if edge from root to node is in search term
                newNode.realEndPoint();//endPoint(", fill = green");
            startNode.addChild(newNode);
            startNode = newNode;
        }
    }

    private static void printFullTreeLatex() {

        tree.append("\\newcommand{\\tab}[1]{\\hspace{.4\\textwidth}\\rlap{#1}}\n" +
                "\\newcommand{\\itab}[1]{\\hspace{0em}\\rlap{#1}}\n");
        tree.append("\\begin{tikzpicture}[every node/.style={draw,circle},\n" + //header for latex
                "   level distance=1.25cm,level 1/.style={sibling distance=2cm}, level 2/.style={sibling distance=1cm},\n" +
                "   edge from parent path={(\\tikzparentnode) -- (\\tikzchildnode)}]\n" +
                "   \\node (root) {root}\n");

        calcFailureLinksLevelbyLevel(root);
        printTreeLatex(root);
        tree.append(";\n\n");
        if (withFailureLinks) {
            printFailureLinks();
        }
        tree.append("\\end{tikzpicture}\n");
    }

    private static void printFailureLinks() {
        for (Node a : failureLinks.keySet()) {
            connectNode(a, failureLinks.get(a));
        }
    }

    private static void calcFailureLinksLevelbyLevel(Node root) {
        failureLinks.put(root, null); //add failure for root
        int level = 1; //beginn at level one
        for (int i = level; i <= depth; i++) {
            calcFailureLinksforLevel(root, i);
        }
    }

    private static void calcFailureLinksforLevel(Node node, int level) {
        if (node == null)
            return;
        if (level == 1)
            for (Node child : node.childs) {
                calcFailureLink(child);
                checkEndpoint(child);
            }
        else if (level > 1) {
            for (Node child : node.childs) {
                calcFailureLinksforLevel(child, level - 1);
            }
        }
    }

    private static void checkEndpoint(Node node) {
        Node child = node;
        if (child.childs.size() == 0) {
            child.realEndPoint();
        } else if (failureLinks.get(child).isEndPoint()) {
            while (!child.isRealEndPoint()) {
                child = failureLinks.get(child);
            }
            node.endPoint("", child);
        }
    }


    private static void calcFailureLink(Node node) {

        Node v = node.parent;
        char edge = node.edge;
        Node w = failureLinks.get(v);

        while (w != null && w.edge != ' ') {
            boolean childWithEdge = false;
            for (Node child : w.childs) {
                if (child.edge == edge) {
                    childWithEdge = true;
                    break;
                }
            }
            if (!childWithEdge) {
                w = failureLinks.get(w);
            } else {
                break;
            }
        }

        Node foundChild = null;
        if (w != null) {
            for (Node child : w.childs) {
                if (child.edge == edge) {
                    foundChild = child;
                }
            }
        }

        if (foundChild != null) {
            failureLinks.put(node, foundChild);
        } else {
            failureLinks.put(node, root);
        }

    }


    private static void connectNode(Node a, Node b) {
        if (a.edge == ' ')
            return;
        tree.append("\\path[->, bend left] (" + a + ") edge node[draw=none]{}(" + b + ");\n");
    }

    private static void printTreeLatex(Node startNode) {
        Node pos = startNode;
        for (Node child : pos.childs) {
            tree.append("child {node [color=" + child.color + ", " + child.thickness + "] (" + child.name + ") {}\n");
            printTreeLatex(child);
        }
        if (pos.edge != ' ')
            tree.append("edge from parent node[midway, left, draw = none, color=black] {$" + pos.edge + "$}}\n");
    }
}

